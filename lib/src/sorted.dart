part of contrast2;

/**
 * Returns a sorted copy of [iterable].
*/
List sorted(Iterable iterable,
        {Comparator<Comparable> compare: Comparable.compare,
        bool growable: true}) =>
    new List.from(iterable, growable: growable)..sort(compare);
